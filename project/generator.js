/*
 * Генератор звонков.
 *
 */
const moment = require('moment');
const settings = require('settings');
const logger = require('utils/logging')('calls');
const appdata = require('utils/data').appdata;
const { newIncoming, saveCall, getOperators } = require('utils/calls');


function randomArray(array) {
  return array[Math.floor(Math.random() * array.length)];
}


async function getCurrentCalls() {
  // logger.debug('getCurrentCalls.');
  const allCalls = await appdata.mget('calls:*');
  const calls = allCalls.filter(call => call.status !== 'finished');
  // logger.debug('current calls %s', calls.length);
  return calls
}


function getTalks(calls) {
  const talks = {};
  calls.filter(c => c.status === 'answered').forEach(c => {
    talks[c.operator] = c.id;
  });
  return talks
}


function getRingings(calls) {
  const ringings = {};
  calls.filter(c => (c.status === 'started' && c.to !== 'queue')).forEach(c => {
    ringings[c.to] = c.id;
  });
  return ringings
}


async function finishAllCalls(io) {
  logger.debug('finishAllCalls().');
  const calls = await getCurrentCalls();
  logger.debug('current calls %s', calls.length);
  for (let i=0; i < calls.length; i++) {
    const call = calls[i];
    call.hangup = moment().format();
    call.status = 'finished';
    call.hangupSystem = true;
    await saveCall(call);
    io.to('calls').emit('calls.call_changed', call);
    logger.warn('Generator call finished %s', call.id);
  }
}


async function createCall(io) {
  // logger.debug('createCall.');
  const calls = await getCurrentCalls();
  if (calls.length >= settings.MAX_CALLS) {
    logger.debug('Maximum calls ("%s") in active.', calls.length);
    // logger.debug(calls);
  }
  else {
    const number = randomArray(settings.INCOMING_NUMBERS);
    const call = newIncoming(number);
    await saveCall(call);
    logger.info('Generator call created %s', call.id);
    io.to('calls').emit('calls.call_created', call);

    const id = call.id;
    const operators = await getOperators();
    const lastOperator = operators.length - 1;
    let operatorIndex = 0;

    const ringToOperator = async function() {
      const call = await appdata.get(`calls:${id}`);
      if (call.status === 'started') {
        const operator = operators[operatorIndex];
        const calls = await getCurrentCalls();
        const talk = getTalks(calls)[operator.username];
        const ring = getRingings(calls)[operator.number];

        if (operatorIndex >= lastOperator) {
          operatorIndex = 0
        }
        else {
          operatorIndex += 1
        }

        if (!talk && !ring) {
          call.to = operator.number;
          await saveCall(call);
          io.to('calls').emit('calls.call_changed', call);
          logger.info(
            'Ring "%s" to "%s" for operator "%s"',
            call.from, call.to, operator.username);
          // Next after timeout.
          setTimeout(ringToOperator, settings.RING_TO_OPERATOR * 1000);
        }
        else if (talk === id) {
          logger.debug(
            'Operator "%s" is answered on current call "%s".',
            operator.username, call.from)
          return;
        }
        else if (ring) {
          logger.debug(
            'The operator "%s" is already being called from "%s".',
            operator.username, ring)
          // Next now!
          setTimeout(ringToOperator, 50);
        }
        else {
          logger.debug(
            'The operator "%s" is talk on "%s".', operator.username, talk);
          // Next now!
          setTimeout(ringToOperator, 50);
        }
      }
    }

    ringToOperator();

    const finish = async function() {
      const call = await appdata.get(`calls:${id}`);
      if (call.status === 'started') {
        call.hangup = moment().format();
        call.status = 'finished';
        call.hangupSystem = true;
        await saveCall(call);
        io.to('calls').emit('calls.call_changed', call);
        logger.warn('Caller "%s" hangup on "%s"', call.from, call.id);
      }
    }

    setTimeout(finish, randomArray([10000, 30000, 50000, 70000]));

  }
}


let enabled = false;


async function start(io) {
  enabled = true;

  function runProcess() {
    if (enabled) {
      const proc = randomArray([
        null,
        createCall,
      ]);
      if (proc) proc(io);
      setTimeout(runProcess, randomArray([1000, 3000, 5000, 7000]));
    }
  }
  setTimeout(runProcess, 3000);
}


async function stop(io) {
  enabled = false;
  logger.info('Work is off.');
  const calls = await getCurrentCalls();
  if (calls.length) {
    logger.info('%s calls not finished yet.', calls.length);
    if (settings.FINISH_CALLS_ON_EXIT) {
      await finishAllCalls(io)
    }
  }
  return true
}


exports.start = start;
exports.stop = stop;
