/*
 * Module returns cache client instance
 *
 */

const redis = require('redis');
const settings = require('settings');

const client = redis.createClient({url: settings.CACHE_REDIS});


/*
 * AppCache constructor
 *
 */
function AppCache() {
  var prefix = 'appcache:';

  /* Каждый перезапуск сервера означает смену кэша, используйте это только
   * для режима разработки.
   */
  if (settings.CACHE_FOR_PID) prefix += process.pid + ':';

  // Функция получения значения.
  this.get = function(key, callback) {
    return new Promise((resolve) => {
      client.get(prefix + key, function(err, reply) {
        // reply is null when the key is missing
        if (reply) {
          try {
            reply = JSON.parse(reply)
          }
          catch (e) {
            reply = null
          }
        }
        if (callback) {
          callback(reply);
        }
        resolve(reply);
      });
    });
  }

  // Функция установки значения (по-умолчанию на 5 минут).
  this.set = function(key, value, seconds, callback) {
    return new Promise((resolve) => {
      var time = seconds || 300;
      var data = JSON.stringify(value);
      client.set(prefix + key, data, function(err, reply) {
        client.expire(prefix + key, time, function() {
          if (callback) {
            callback(reply);
          }
          resolve(reply);
        });
      });
    });
  }

  // Функция очистки кэша.
  this.clean = function(keystart, callback) {
    return new Promise((resolve) => {
      var search = prefix;
      if (keystart) {
        search += keystart
      }
      var list_keys = [];
      client.keys(search + '*', function(err, keys) {
        keys.forEach(function(key) { list_keys.push(key) });
        client.del(list_keys, function() {
          if (callback) {
            callback(list_keys);
          }
          resolve(list_keys);
        });
      })
    });
  }
}


exports.client = client;
exports.appcache = new AppCache();
