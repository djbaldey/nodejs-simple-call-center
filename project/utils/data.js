/*
 * Module returns cache client instance
 *
 */

const redis = require('redis');
const settings = require('settings');

const client = redis.createClient({url: settings.DATA_REDIS});


/*
 * AppData constructor
 *
 */
function AppData() {
  var prefix = 'appdata:';

  // Функция получения одиночного значения.
  this.get = function(key, callback) {
    return new Promise((resolve) => {
      client.get(prefix + key, function(err, reply) {
        // reply is null when the key is missing
        if (reply) {
          try {
            reply = JSON.parse(reply)
          }
          catch (e) {
            reply = null
          }
        }
        if (callback) {
          callback(reply);
        }
        resolve(reply);
      });
    });
  }

  // Функция получения нескольких значений.
  this.mget = function(filter, callback) {
    return new Promise((resolve) => {
      client.keys(prefix + filter, function (error, keys) {
        client.mget(keys, function (err, values) {
          const L = [];
          if (values) {
            for (let i = 0; i < values.length; i++) {
              let value = values[i]
              if (value) {
                try {
                  L.push(JSON.parse(value));
                }
                catch (e) {
                  // pass
                }
              }
            }
          }
          if (callback) {
            callback(L);
          }
          resolve(L);
        });
      });
    });
  }

  // Функция установки значения.
  this.set = function(key, value, callback) {
    return new Promise((resolve) => {
      var data = JSON.stringify(value);
      client.set(prefix + key, data, function(err, reply) {
        if (callback) {
          callback(reply);
        }
        resolve(reply);
      });
    });
  }

  // Функция установки истечения хранения данных.
  this.expire = function(key, time, callback) {
    return new Promise((resolve) => {
      client.expire(prefix + key, time, function(err, reply) {
        if (callback) {
          callback(reply);
        }
        resolve(reply);
      });
    });
  }

  // Функция очистки данных.
  this.clean = function(keymask='*', callback) {
    return new Promise((resolve) => {
      var list_keys = [];
      client.keys(prefix + keymask, function(err, keys) {
        keys.forEach(function(key) { list_keys.push(key) });
        client.del(list_keys, function() {
          if (callback) {
            callback(list_keys);
          }
          resolve(list_keys);
        });
      })
    });
  }
}


exports.client = client;
exports.appdata = new AppData();
