/*
 * Module returns functions for calls.
 *
 */
const { v4: uuidv4 } = require('uuid');
const moment = require('moment');
const appdata = require('utils/data').appdata;


function newCall(extra) {
  return {
    id: uuidv4(),
    created: moment().format(),
    status: 'started',
    ...extra,
  }
}


function newIncoming(number) {
  return newCall({
    direction: 'incoming',
    from: number,
    to: 'queue',
  })
}


function newOutgoing(number, operator) {
  return newCall({
    direction: 'outgoing',
    operator: operator.username,
    from: operator.number,
    to: number,
  })
}


// Любое сохранение звонка запоминается на сутки, потом звонок исчезает из базы.
function saveCall(call, callback) {
  const key = `calls:${call.id}`;
  return appdata.set(key, call, function(error, reply) {
    appdata.expire(key, 86400, () => {
      if (callback) callback(error, reply);
    });
  });
}


async function getOperators() {
  let operators = await appdata.mget('users:*');
  operators = operators.filter(user => !!user.number);
  operators.sort((a, b) => {
    if (a.number < b.number) {
      return -1;
    }
    if (a.number > b.number) {
      return 1;
    }
    return 0;
  });
  return operators
}


exports.newIncoming = newIncoming;
exports.newOutgoing = newOutgoing;
exports.saveCall = saveCall;
exports.getOperators = getOperators;
