/*
 * Module returns logger instance by name
 *
 */

const tracer = require('tracer');
const settings = require('settings');


function dummy() {}

const format = '[{{timestamp}}] <{{title}}> {{file}}:{{line}} -- {{message}}';
const dateformat = 'yyyy-mm-dd HH:MM:ss';


module.exports = function (name) {
  if (name === 'dummy') {
    return {
      log: dummy,
      trace: dummy,
      debug: dummy,
      info: dummy,
      warn: dummy,
      error: dummy,
    }
  }
  if (name) {
    const logger = settings.LOGGERS[name];
    if (typeof (logger) == 'string') {
      return tracer.colorConsole({
        format,
        dateformat,
        level: logger,
      })
    } else if (logger) {
      return logger
    }
  }
  return tracer.colorConsole({
    format,
    dateformat,
    level: settings.DEFAULT_LOGGER_LEVEL,
  })
};
