/*
 * Main http app.
 *
 */
const express = require('express');
const path = require('path');

const app = express();
const publicPath = path.join(process.cwd(), 'public');

app.use(express.static(publicPath));
app.set('views', publicPath);
app.set('view engine', 'html');
app.get('/', function (req, res) {
  res.status(200).render('index');
});


module.exports = app;
