/*
 * Файл настроек по умолчанию.
 * Все настройки перезаписываются файлом ../secretsettings.js, который может
 * отсутствовать вовсе и должен быть исключён из хранения в репозитории.
 *
 */

// Общий дебаг-режим.
exports.DEBUG = true;
// Общий уровень логгирования работы всех модулей проекта.
exports.DEFAULT_LOGGER_LEVEL = 'debug';
// Карта подключения именованных специализированных логгеров tracer.
// Например: {'http/app': 'info'} - установит консольный логгер уровня отладки.
// См. подробнее https://www.npmjs.com/package/tracer
exports.LOGGERS = {};


// Сервер запуска Node.js.
exports.SERVER = {
  HOST: 'localhost',
  PORT: 3000,
};

// Сокеты в базу №0. Настройка для модуля `socket.io-redis`.
exports.SOCKET_REDIS = {
  HOST: 'localhost',
  PORT: 6379,
};
// Кэши и данные в базу №1. Настройка для модуля `redis`.
exports.DATA_REDIS = 'redis://localhost:6379/1';
exports.CACHE_REDIS = 'redis://localhost:6379/1';


// Максимальное количество одновременных звонков.
exports.MAX_CALLS = 2;
// Количество секунд дозвона до оператора.
exports.RING_TO_OPERATOR = 10;
// Завершать звонки при завершении работы сервера.
exports.FINISH_CALLS_ON_EXIT = true;

// Перечень номеров входящих номеров телефонов.
const numbers = [];
for (let n=0; n < 1000; n++) {
  numbers.push(`+${79008000000 + n}`);
}
exports.INCOMING_NUMBERS = numbers;

// Переопределение дефолтных настроек из файла "secretsettings.js".
try {
  const secrets = require('../secretsettings');
  for (var k in secrets) {
    exports[k] = secrets[k];
  }
} catch (e) {
  /* eslint-disable */
  console.warn('File "secretsettings.js" not found or bug.');
  /* eslint-enable */
}
