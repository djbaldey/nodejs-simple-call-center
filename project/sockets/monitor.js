/*
 * Monitor API
 *
 */
const settings = require('settings');
const logger = require('utils/logging')('monitor');

/*
 * Подключение обработчиков сокетов.
 *
 */
function connect(socket) {
  socket.on('monitor.subscribe', function(key) {
    logger.debug('monitor.subscribe', key);
    if (settings.MONITOR_KEY && settings.MONITOR_KEY !== key) {
      socket.emit('monitor.subscribe', false, 'You are not an ');
    }
    else {
      socket.join('monitor');
      // Извещаем пользователей мониторинга.
      socket.to('monitor').emit('monitor.connected', socket.id);
      socket.emit('monitor.connected', socket.id);
      socket.emit('monitor.subscribe', true);
    }
  })

  socket.on('disconnect', function() {
    // Извещаем пользователей мониторинга.
    socket.to('monitor').emit('monitor.diconnected', socket.id);
    socket.emit('monitor.diconnected', socket.id);
  })
}


exports.connect = connect;
