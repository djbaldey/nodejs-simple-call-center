/*
 * Users API
 *
 */
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const appdata = require('utils/data').appdata;
const logger = require('utils/logging')('users');


/* Функция проверки доступа в раздел звонков. */
function hasAccess(socket) {
  return socket && socket.getSession().is_authenticated
}


// Сериализация объекта пользователя для передачи клиенту.
function serializeUser(data) {
  // eslint-disable-next-line
  const { password, ...user } = data;
  return user;
}


// Сериализация сокет-пользователя для передачи клиенту.
function serializeSocketUser(socket) {
  const session = socket.getSession();
  const user = serializeUser(socket.getUser());
  user.is_authenticated = session.is_authenticated;
  user.token = session.token;
  user.token_expired = session.token_expired;
  return user;
}


function createUsers() {
  // Создадим несколько пользователей.
  for (let i=1; i <= 7; i++) {
    appdata.set(`users:user${i}`, {
      username: `user${i}`,
      password: `user${i}`,
      number: `00${i}`,
    })
  }
}


// Аутентификация по токену продляет его срок жизни.
async function login(socket, user, tokenData) {
  const session = socket.getSession();
  session.user = user;
  session.is_authenticated = Boolean(user.username);
  if (session.is_authenticated) {
    const expired = moment().add(86400, 'seconds').format();
    if (!tokenData) {
      tokenData = {
        username: user.username,
        token: uuidv4(),
        expired,
      }
    }
    else {
      tokenData.expired = expired;
    }
    session.token = tokenData.token;
    session.token_expired = tokenData.expired;
    const key = `tokens:${tokenData.token}`;
    await appdata.set(key, tokenData);
    await appdata.expire(key, 86400);
    user.online = moment().format();
    await appdata.set(`users:${user.username}`, user);
    socket.to('monitor').emit('users.user_online', user.username);
    if (user.number) {
      // Отправляем оператора.
      socket.to('calls').emit('calls.operator_online', user.username);
    }
    logger.info(`${user.username} is login`);
  }
  else {
    logger.warn(`Anonymous is connected.`);
  }
  return session
}


// Разрыв соединения сокета не удаляет токен из базы, а действие, произведённое
// пользователем, удаляет.
async function logout(socket, removeToken) {
  const session = socket.getSession();
  if (session.is_authenticated) {
    const user = socket.getUser();
    logger.info(`${user.username} is logout`);
    session.is_authenticated = false;
    session.user = {};
    if (removeToken) {
      await appdata.clean(`tokens:${session.token}`);
      logger.info(`Token "${session.token}" is removed.`);
    }
    session.token = undefined;
    session.token_expired = undefined;

    user.online = null;
    user.offline = moment().format();
    await appdata.set(`users:${user.username}`, user);
    socket.to('monitor').emit('users.user_offline', user.username);
    if (user.number) {
      // Отправляем оператора.
      socket.to('calls').emit('calls.operator_offline', user.username);
    }
  }
  return session
}


/*
 * Подключение обработчиков сокетов.
 *
 */
function connect(socket) {

  // Аутентифицирует пользователя по токену или логин-паролю.
  socket.on('users.login', async function({ username, password, token }, callback) {
    const session = socket.getSession();
    if (session.is_authenticated) {
      socket.emit('users.login', serializeSocketUser(socket));
      logger.warn(
        'The user has already been authenticated before.'
      );
      return
    }

    let user = null, error;

    function sendUser() {
      if (callback) {
        callback(user, error)
      }
      else {
        socket.emit('users.login', user, error);
      }
    }

    // Первичная аутентификация по имени и паролю.
    if (username && password) {
      const userData = await appdata.get(`users:${username}`)
      if (password && userData && password === userData.password) {
        await login(socket, userData);
        user = serializeSocketUser(socket);
      }
      else {
        error = 'Credentials is not valid.';
        logger.error(error);
      }
      sendUser()
    }
    // Аутентификация по токену, который выдаётся в результате аутентификации
    // по имени и паролю. Токен должен храниться на клиенте, например в
    // SessionStorage и передаваться при переустановке разорванного соединения.
    else if (token) {
      const tokenData = await appdata.get(`tokens:${token}`);
      if (tokenData && tokenData.username) {
        const userData = await appdata.get(`users:${tokenData.username}`);
        if (userData) {
          await login(socket, userData, tokenData);
          user = serializeSocketUser(socket);
        }
        else {
          error = 'The user has been renamed or removed.';
          logger.error(error);
        }
        sendUser()
      }
      else {
        error = 'The token has expired.';
        sendUser()
        logger.error(error);
      }
    }
    else {
      logger.error(
        'The authentication data is not transmitted by the client side.'
      )
    }
  })

  // Деаутентифицирует пользователя с удалением токена из базы.
  socket.on('users.logout', async function() {
    await logout(socket, true)
    socket.emit('users.logout', serializeSocketUser(socket));
  });

  // Возвращает перечень всех онлайн-пользователей из хранилища.
  socket.on('users.online', async function(callback) {
    if (hasAccess(socket)) {
      let users = await appdata.mget('users:*');
      users = users.filter(user => !!user.online);
      users.sort((a, b) => {
        if (a.username < b.username) {
          return -1;
        }
        if (a.username > b.username) {
          return 1;
        }
        return 0;
      });

      users = users.map(user => serializeUser(user));

      if (callback) {
        callback(users)
      }
      else {
        socket.emit('users.online', users);
      }
    }
  })
}


exports.createUsers = createUsers;
exports.login = login;
exports.logout = logout;
exports.connect = connect;
