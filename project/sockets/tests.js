/*
 * Tests API
 *
 */

/*
 * Подключение обработчиков сокетов.
 *
 */
function connect(socket) {

  socket.on('tests.message', function() {
    // No errors:
    socket.emit('tests.message', [1, 2, 3])
    socket.emit('tests.message', {a:1})
    socket.emit('tests.message', 'data as string')
    // Send error:
    socket.emit('tests.message', false, 'Error data')
    socket.emit('tests.message', null, 'Error data')
    socket.emit('tests.message', undefined, 'Error data')
  })

  socket.on('tests.args', function(arg1, arg2, callback) {
    var data = {arg1: arg1, arg2: arg2, callback: callback};
    if (callback) {
      callback(data)
    }
    else {
      socket.emit('tests.args', data)
    }
  })
}


exports.connect = connect;
