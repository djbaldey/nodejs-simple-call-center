/*
 * Calls API
 *
 */
const moment = require('moment');
const appdata = require('utils/data').appdata;
const logger = require('utils/logging')('calls');
const { newOutgoing, saveCall, getOperators } = require('utils/calls');


/* Функция проверки доступа в раздел звонков. */
function hasAccess(socket) {
  return socket && socket.getSession().is_authenticated
}


/*
 * Подключение обработчиков сокетов.
 *
 */
function connect(socket) {

  socket.join('calls');

  // Возвращает перечень всех операторов из хранилища.
  socket.on('calls.operators', async function(callback) {
    if (hasAccess(socket)) {
      let operators = await getOperators();
      operators = operators.map(u => ({
        username: u.username,
        number: u.number,
        online: u.online,
      }))
      if (callback) {
        callback(operators)
      }
      else {
        socket.emit('calls.operators', operators);
      }
    }
  })

  // Возвращает абсолютно все звонки из хранилища.
  socket.on('calls.all', async function(callback) {
    if (hasAccess(socket)) {
      const calls = await appdata.mget('calls:*');
      calls.sort((a, b) => {
        if (a.created < b.created) {
          return -1;
        }
        if (a.created > b.created) {
          return 1;
        }
        return 0;
      });

      if (callback) {
        callback(calls)
      }
      else {
        socket.emit('calls.all', calls);
      }
    }
  })

  // Возвращает все активные (незавершённые) звонки.
  socket.on('calls.active', async function(callback) {
    if (hasAccess(socket)) {
      let calls = await appdata.mget('calls:*');
      calls = calls.filter(call => call.status !== 'finished');
      calls.sort((a, b) => {
        if (a.created < b.created) {
          return -1;
        }
        if (a.created > b.created) {
          return 1;
        }
        return 0;
      });

      if (callback) {
        callback(calls)
      }
      else {
        socket.emit('calls.active', calls);
      }
    }
  })

  socket.on('calls.get', async function(call_id, callback) {
    if (hasAccess(socket)) {
      const call = appdata.get(`calls:${call_id}`);
      if (callback) {
        callback(call)
      }
      else {
        socket.emit('calls.get', call);
      }
    }
  })

  socket.on('calls.make', async function(number) {
    if (hasAccess(socket)) {
      const user = socket.getUser();
      if (!user) {
        logger.error('The socket has no user.');
        return
      }
      if (!user.number) {
        logger.error('The user has no number.');
        return
      }
      const call = newOutgoing(number, user);
      call.operator = user.username;
      await saveCall(call);
      socket.to('calls').emit('calls.call_created', call);
      socket.emit('calls.call_created', call);
      logger.warn('Operator "%s" make going "%s"', call.operator, call.id)
    }
  })

  socket.on('calls.answer', async function(call_id) {
    if (hasAccess(socket)) {
      const call = await appdata.get(`calls:${call_id}`);
      if (call && call.status === 'started') {
        const user = socket.getUser();
        call.operator = user.username;
        call.answered = moment().format();
        call.status = 'answered';
        await saveCall(call);
        socket.to('calls').emit('calls.call_changed', call);
        socket.emit('calls.call_changed', call);
        logger.info('Operator "%s" answered on "%s"', call.operator, call.id)
      }
    }
  })

  socket.on('calls.hangup', async function(call_id) {
    if (hasAccess(socket)) {
      const call = await appdata.get(`calls:${call_id}`);
      if (call && call.status !== 'finished') {
        const user = socket.getUser();
        call.operator = user.username;
        call.hangup = moment().format();
        call.status = 'finished';
        call.hangupOperator = true;
        await saveCall(call);
        socket.to('calls').emit('calls.call_changed', call);
        socket.emit('calls.call_changed', call);
        logger.warn('Operator "%s" hangup on "%s"', call.operator, call.id);
      }
    }
  })

}


exports.connect = connect;
