/*
 * Main app for sockets API
 *
 */
const logger = require('utils/logging')('sockets');
// Подключаем внутренние модули
const calls = require('sockets/calls');
const monitor = require('sockets/monitor');
const tests = require('sockets/tests');
const users = require('sockets/users');


/*
 * App constructor
 *
 */
function App() {

  var app = this;

  // Запуск работы приложения.
  app.listen = function(io) {
    // Запоминаем в приложении объект массовой рассылки IO.
    app.io = io;

    users.createUsers();

    // Обработчик сокета при присоединении.
    function onConnection(socket) {
      // Подключаем к сокету разные функции получения данных о соединении
      // для удобства написания кода
      socket.getSession = function() {
        if (!socket.session) {
          socket.session = {};
        }
        return socket.session
      }
      socket.isAuth = function() {
        return socket.getSession().is_authenticated;
      }
      socket.getUser = function() {
        return socket.getSession().user || {}
      }
      socket.getUsername = function() {
        return socket.getUser().username
      }
      socket.getUserID = function() {
        return socket.getUser().username
      }

      // Хранилище информации о пользователе, его правах, ключах доступа
      // и прочей нужной лабуды: "socket.session".
      // Заполняем хранилище неавторизованным пользователем,
      // и ждём явного запроса авторизации с клиента.
      users.login(socket, {});

      // Подключение обработчиков сокета в различных приложениях.
      calls.connect(socket);
      monitor.connect(socket);
      tests.connect(socket);
      users.connect(socket);

      // TODO: подключение остальных обработчиков расположить здесь...

      // Обработчик сокета при обрыве соединения.
      function onDisconnect() {
        // При разрыве соединения пишем в консоль при включённом дебаге
        logger.info('socket disconnected: %s', socket.id);
        // Разлогиниваемся с оповещением подсистемы авторизации.
        users.logout(socket);
      }

      socket.on('disconnect', onDisconnect);
      // При запуске соединения пишем в консоль при включённом дебаге
      logger.info('socket connected: %s', socket.id);
    }

    io.on('connection', onConnection);
  }
}

module.exports = new App();
