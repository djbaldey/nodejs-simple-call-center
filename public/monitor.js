/*
 * monitor.js
 *
 */

const socket = io(window.location.origin);
const monitor = document.getElementById('monitor');
const state = document.getElementById('state');
const login = document.getElementById('login');
const loginError = document.getElementById('login-error');
const logout = document.getElementById('logout');
const action = document.getElementById('action');
let user = {};
let currentCall = null;


function renderMessage(message, type) {
  const child = document.createElement('div');
  child.appendChild(document.createTextNode(message));
  child.classList.add(`text-${type || 'muted'}`)
  monitor.appendChild(child)
}


function autoLogin() {
  const token = localStorage.token;
  if (token) {
    socket.emit('users.login', { token });
  }
}


function formLogin(e) {
  e.preventDefault();
  const formData = new FormData(event.currentTarget);
  console.log(formData)
  socket.emit('users.login', {
    username: formData.get('username'),
    password: formData.get('password'),
  });
}


function renderAction() {
  if (currentCall && currentCall.status !== 'finished') {
    action.hidden = false;
    action.innerHTML = currentCall.status === 'started'
      ? `Answer ${currentCall.from}`
      : `Hangup ${currentCall.from}`;
  }
  else {
    action.hidden = true;
  }
}


function receivedCall(call) {
  if (call) {
    let type = 'muted', status = call.status;
    if (status === 'started' && call.to === 'queue') {
      type = 'danger';
    }
    else if (status === 'started') {
      type = 'info';
      status = 'ringing';
    }
    else if (status === 'answered') {
      type = 'success';
    }
    else if (status === 'finished') {
      type = 'danger';
    }

    // console.log(call);

    // Отображаем кнопки для входящего звонка.
    if (call.to === user.number) {
      currentCall = status !== 'finished' ? call : null;
      renderAction();
    }
    // Текущий звонок перешёл на другого оператора.
    else if (currentCall && currentCall.id === call.id) {
      currentCall = null;
      renderAction();
    }

    renderMessage(
      `${call.direction} ${status}: ${call.from} >> ${call.to} -- (${call.id})`,
      type
    )
  }
}


function doAction(e) {
  if (currentCall) {
    const { status, id } = currentCall;
    if (status === 'started') {
      socket.emit('calls.answer', id);
      console.log('answer', id)
    }
    else if (status === 'answered') {
      socket.emit('calls.hangup', id);
      console.log('hangup', id)
    }
  }
}


login.addEventListener("submit", formLogin);
logout.addEventListener("click", () => {
  socket.emit('users.logout');
});
action.addEventListener("click", doAction);

socket.on('calls.active', calls => {
  if (user.number) {
    calls.forEach(call => {
      if (call.to === user.number) {
        currentCall = call;
        renderAction();
      }
    })
  }
});
socket.on('calls.call_changed', receivedCall);
socket.on('calls.call_created', receivedCall);
socket.on('calls.operator_online', username => {
  renderMessage(`Operator "${username}" is online.`)
});
socket.on('calls.operator_offline', username => {
  renderMessage(`Operator "${username}" is offline.`)
});
socket.on('calls.operators', operators => {
  renderMessage(`All operators: ${operators.map(i => i.username).join(', ')}.`)
});
socket.on('connect', function(data) {
  monitor.innerHTML = '';
  state.innerHTML = '<p class="my-0">Socket is connected!</p>';
  state.classList.add('alert-info');
  state.classList.remove('alert-danger', 'alert-success');
  autoLogin();
  // Задержка для прочтения на экране.
  setTimeout(function() {socket.emit('monitor.subscribe')}, 1000);
});
socket.on('disconnect', function() {
  state.innerHTML = '<p class="my-0">Socket is diconnected!</p>';
  state.classList.add('alert-danger');
  state.classList.remove('alert-info', 'alert-success');
});
socket.on('monitor.connected', function(id) {
  if (id === socket.id) id = '';
  renderMessage(`connected ${id} to monitor.`)
});
socket.on('monitor.diconnected', function(id) {
  if (id === socket.id) id = '';
  renderMessage(`diconnected ${id} from monitor.`)
});
socket.on('monitor.subscribe', function(data, error) {
  let text;
  if (!data) {
    text = error || 'Subscribing is broken.'
    console.error(text);
    state.classList.add('alert-danger');
    state.classList.remove('alert-info', 'alert-success');
  }
  else {
    text = 'Monitoring is ready!';
    state.classList.add('alert-success');
    state.classList.remove('alert-danger', 'alert-info');
  }
  state.innerHTML = `<p class="my-0">${text}</p>`;
  socket.emit('users.online');
  socket.emit('calls.operators');
});
socket.on('users.login', (data, error) => {
  if (error) {
    console.error(error);
    loginError.innerHTML = `<p class="mb-0">${error}</p>`;
    loginError.hidden = false;
    login.hidden = false;
    logout.hidden = true;
    user = {};
  }
  else {
    login.hidden = true;
    loginError.hidden = true;
    logout.hidden = false;
    localStorage.token = data.token;
    user = data;
    socket.emit('calls.active');
  }
});
socket.on('users.logout', (data, error) => {
  if (error) {
    console.error(error);
  }
  login.hidden = false;
  loginError.hidden = true;
  logout.hidden = true;
  user = {};
});
socket.on('users.online', users => {
  renderMessage(`Online users: ${users.map(i => i.username).join(', ')}.`)
});
socket.on('users.user_online', username => {
  renderMessage(`User "${username}" is online.`)
});
socket.on('users.user_offline', username => {
  renderMessage(`User "${username}" is offline.`)
});
