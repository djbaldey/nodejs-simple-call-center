/*
 * Server module.
 *
 */
require('app-module-path').addPath(__dirname + '/project');

const http = require('http');
const settings = require('settings');
const httpApp = require('http/app');
const server = http.Server(httpApp);
const io = require('socket.io')(server);
const generator = require('generator');
const socketsApp = require('sockets/app');
const socket_redis = require('socket.io-redis');
const logger = require('utils/logging')('server');

const PORT = process.argv[2] || settings.SERVER.PORT;
const HOST = settings.SERVER.HOST;

if (settings.SOCKET_REDIS) {
  io.adapter(socket_redis({
    host: settings.SOCKET_REDIS.HOST,
    port: settings.SOCKET_REDIS.PORT,
  }));
}

socketsApp.listen(io);
generator.start(io);

// Запуск сервера
/* eslint-disable */
server.listen(PORT, HOST, function () {
  console.info(
    `Server running at http://${HOST}:${PORT}/`
  );
});


// Блокировка краша сервера с выводом ошибок в консоль.
process.on('uncaughtException', function (err) {
  logger.error(err.stack);
});


// Обработка чего-либо до завершения работы сервера.
async function exit(signal) {
  console.info(
    `\b\bServer stopped at http://${HOST}:${PORT}/ by ${signal}`
  );
  // Закрываем все сокет-соединения как положено.
  await generator.stop(io);
  io.close();
  process.exit();
}

process.on('SIGINT', exit);
process.on('SIGTERM', exit);
